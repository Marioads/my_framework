<?php
  require 'controllers/usersControllers/UsuariosController.php';
  $controller = new Usuarios_Controller();
 ?>


 <div class="container">
   <h1>UPDATE</h1>
   <form style="width: 300px;" action="controllers/usersControllers/updateController.php" method="post">
     <div class="form-group">
       <label for="id">ID</label>
       <input type="text" class="form-control" id="id" name="id" placeholder="Ingresa el id">
     </div>
     <div class="form-group">
       <label for="nombre">Nombre</label>
       <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Ingresa tu nombre">
     </div>
     <div class="form-group">
       <label for="email">Correo</label>
       <input type="email" class="form-control" id="email" name="correo" placeholder="Correo">
     </div>
     <div class="form-group">
       <label for="password">Password</label>
       <input type="password" class="form-control" id="password" name="password" placeholder="Password">
     </div>
     <button type="submit" class="btn btn-primary">Actualizar</button>
   </form>
 </div>
