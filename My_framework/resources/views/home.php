<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>My framework</title>
    <link rel="stylesheet" href="resources/CSS/bootstrap.css">
  </head>
  <body>
      <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="index.php">My Framework</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
          <ul class="navbar-nav">
            <li class="nav-item dropdown" id="menuUsuarios">
              <a class="nav-link" href="#" id="navbarUsuarios" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Usuario
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarUsuarios">
                <a class="dropdown-item" href="index.php?option=1">Insert</a>
                <a class="dropdown-item" href="index.php?option=2">Delete</a>
                <a class="dropdown-item" href="index.php?option=3">Update</a>
              </div>
            </li>
            <li class="nav-item dropdown" id="menuProductos">
              <a class="nav-link" href="#" id="navbarProductos" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Productos
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarProductos">
                <a class="dropdown-item" href="index.php?option=4">Insert</a>
                <a class="dropdown-item" href="index.php?option=5">Delete</a>
                <a class="dropdown-item" href="index.php?option=6">Update</a>
              </div>
            </li>
          </ul>
        </div>
      </nav>

  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  <script src="resources/JS/functions.js"></script>
  </body>
</html>
