<?php
  require 'controllers/productosControllers/ProductosController.php';
  $controller = new Productos_Controller();
  $datos =  $controller->getUsers();
  $dat = json_decode($datos,true);
 ?>

 <div class="container" style="width:90px; margin-left: 0px;">
      <h1>SELECT</h1>
      <table class="table">
      <thead>
        <tr>
          <th scope="col">Id</th>
          <th scope="col">Descripcion</th>
          <th scope="col">Precio</th>
          <th scope="col">Cantidad</th>
        </tr>
      </thead>
      <tbody>
        <?php
        if ($dat > 1) {
          foreach($dat as $dato){
            echo "<tr>";
               echo "<td>$dato[id]</td>";
               echo "<td>$dato[descripcion]</td>";
               echo "<td>$dato[precio]</td>";
               echo "<td>$dato[cantidad]</td>";
               echo '<td><a href="controllers/productosControllers/deleteController.php?id='.$dato["id"].'" class="btn btn-danger btn-sm">Eliminar</a></td>';
            echo "</tr>";
          }
        }
         ?>
      </tbody>
    </table>
  </div>
