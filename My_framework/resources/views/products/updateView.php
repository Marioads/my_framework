<?php
  require 'controllers/productosControllers/ProductosController.php';
  $controller = new Productos_Controller();
 ?>


 <div class="container">
   <h1>UPDATE</h1>
   <form style="width: 300px;" action="controllers/productosControllers/updateController.php" method="post">
     <div class="form-group">
       <label for="id">ID</label>
       <input type="text" class="form-control" id="id" name="id" placeholder="Ingresa el id">
     </div>
     <div class="form-group">
       <label for="descripcion">Descripcion</label>
       <input type="text" class="form-control" id="descripcion" name="descripcion" placeholder="Ingresa la descripcion del producto">
     </div>
     <div class="form-group">
       <label for="precio">Precio</label>
       <input type="number" class="form-control" id="precio" name="precio" placeholder="0" min="0">
     </div>
     <div class="form-group">
       <label for="cantidad">Cantidad</label>
       <input type="number" class="form-control" id="cantidad" name="cantidad" placeholder="unidades" min="0">
     </div>
     <button type="submit" class="btn btn-success">Actualizar</button>
   </form>
 </div>
