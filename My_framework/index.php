<?php
  require 'controllers/HomeController.php';
  $index = new HomeController();
  $index->load();
  $opcion = 0;

  if (isset($_GET['option'])) {
    $opcion = $_GET['option'];
    switch ($opcion) {
      case '1': include('./resources/views/users/insertView.php'); break;
      case '2': include('./resources/views/users/deleteView.php'); break;
      case '3': include('./resources/views/users/updateView.php'); break;
      case '4': include('./resources/views/products/insertView.php'); break;
      case '5': include('./resources/views/products/deleteView.php'); break;
      case '6': include('./resources/views/products/updateView.php'); break;
    }
  }

 ?>
