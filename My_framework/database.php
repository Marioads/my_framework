<?php

    class Database {
      private $host;
      private $user;
      private $pass;
      private $db_name;

      public function __construct (){
        $this->host = 'localhost';
        $this->user = 'root';
        $this->pass = '';
        $this->db_name = 'tienda';
      }

      public function conectar (){
        $connection = new mysqli($this->host,$this->user,$this->pass,$this->db_name);
        if ($connection->connect_error){
          echo 'Ocurrio un error';
        }else {
          $connection->query("SET NAMES 'utf8'");
          return $connection;
        }
      }
    }

 ?>
