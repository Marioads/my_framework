<?php

  class Main {

    public function __construct (){
      //codigo del constructor
    }

    protected function loadView ($view){
      $path = 'resources/views/'.$view.'.php';

      if ( file_exists($path) ) {
        include $path;
      } else {
        echo 'No existe la ruta';
      }
    }
  }

 ?>
