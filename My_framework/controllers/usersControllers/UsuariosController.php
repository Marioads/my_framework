<?php

  require 'models/UsuariosModel.php';

  class Usuarios_Controller {

    protected $usuario;

    public function __construct (){}

    public function getUsers (){
      $this->usuario = new Usuarios();
      $result = $this->usuario->find();

      return $result;
    }

    public function registrar (){

      if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $datos = [
          'nombre' => trim($_POST['nombre']),
          'correo' => trim($_POST['correo']),
          'password' => trim($_POST['password'])
        ];
        $this->usuario->add($datos);
      }else {
        die('Error al registrar');
      }
    }

  }

 ?>
