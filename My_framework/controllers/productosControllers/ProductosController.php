<?php

  require 'models/ProductosModel.php';

  class Productos_Controller {

    protected $producto;

    public function __construct (){}

    public function getUsers (){
      $this->producto = new Productos();
      $result = $this->producto->find();

      return $result;
    }

    public function registrar (){

      if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $datos = [
          'descripcion' => trim($_POST['descripcion']),
          'precio' => trim($_POST['precio']),
          'cantidad' => trim($_POST['cantidad'])
        ];
        $this->usuario->add($datos);
      }else {
        die('Error al registrar');
      }
    }

  }

 ?>
